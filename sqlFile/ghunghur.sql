-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2019 at 01:06 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ghunghur`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$c/F1rddue3aEKK7cvURgZepANSuLudHv7DRr6MyWYz/1S1No865tm', 'nDAKHZgGGCsAua7tP4c1bbpwRpL6tuK19O8fvA0ZReAV1Vhkq8Tsm1KgkoZX', NULL, NULL),
(2, 'tanvir', 'user@gmail.com', NULL, '$2y$10$c/F1rddue3aEKK7cvURgZepANSuLudHv7DRr6MyWYz/1S1No865tm', 'lTXgYQjvNAn0MORiQSuo6Iuli6YttGeItWbhMgQpjlytOSrtm9podtgegJpf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `articale_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `favourite` int(6) NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `author_name`, `title`, `published_date`, `articale_body`, `date`, `favourite`, `image`, `status`, `created_at`, `updated_at`) VALUES
(10, 'মীর আরিফ', 'পুলিৎজার বিজয়ী অ্যাডাম জনসনের সাক্ষাৎকার', 'নভেম্বর ১৬, ২০১৮', '<p><strong>ঢাকা লিটফেস্টে অংশগ্রহণ করে কেমন বোধ করছেন? বাংলাদেশে এটাই কি আপনার প্রথম ভ্রমণ?</strong></p>\r\n\r\n<p>হ্যাঁ, এটাই প্রথম বাংলাদেশে আসা। ঢাকা অপূর্ব সুন্দর শহর। আমি এই ধরনের সাহিত্য আসর খুব পছন্দ করি। আমি মনে করি, এই ধরনের সম্প্রদায়কে একসঙ্গে জড়ো করে তাদের বক্তব্যের উপর জোর ও সত্য উচ্চারণে সুযোগ করে দেওয়া একটা বিরাট সম্পদ হয়ে থাকবে। এটার অংশ হতে পেরে আমি গর্বিত।</p>', '2019-01-28 09:47:08', 0, '1548840615.png', 1, NULL, NULL),
(12, 'মীর আরিফ', 'পুলিৎজার বিজয়ী অ্যাডাম জনসনের সাক্ষাৎকার', 'নভেম্বর ১৬, ২০১৮', '<p>হ্যাঁ, এটাই প্রথম বাংলাদেশে আসা। ঢাকা অপূর্ব সুন্দর শহর। আমি এই ধরনের সাহিত্য আসর খুব পছন্দ করি। আমি মনে করি, এই ধরনের সম্প্রদায়কে একসঙ্গে জড়ো করে তাদের বক্তব্যের উপর জোর ও সত্য উচ্চারণে সুযোগ করে দেওয়া একটা বিরাট সম্পদ হয়ে থাকবে। এটার অংশ হতে পেরে আমি গর্বিত।</p>', '2019-01-30 04:48:31', 1, '1548823711.png', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log_admin`
--

CREATE TABLE `log_admin` (
  `id` int(11) NOT NULL,
  `adminid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `ip_history` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_ip_add_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `del_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_ip_del_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sip_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sip_ip_history` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sip_ip_add_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `del_sip_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sip_delete_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `signature` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `log_admin`
--

INSERT INTO `log_admin` (`id`, `adminid`, `ip`, `ip_history`, `log_ip_add_date`, `del_ip`, `log_ip_del_date`, `sip_ip`, `sip_ip_history`, `sip_ip_add_date`, `del_sip_ip`, `sip_delete_date`, `status`, `signature`, `created_at`, `updated_at`) VALUES
(1, 'admin', '127.0.0.1', NULL, '2018-11-25 03:00:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(2, 'admin', '127.0.0.1', NULL, '2018-11-25 22:04:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(3, 'admin', '127.0.0.1', NULL, '2018-11-25 22:23:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(4, 'admin', '127.0.0.1', NULL, '2018-11-26 02:24:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(5, 'admin', '192.168.0.111', NULL, '2018-11-26 06:03:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(6, 'admin', '127.0.0.1', NULL, '2018-11-26 21:47:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(7, 'admin', '127.0.0.1', NULL, '2018-11-27 05:54:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(8, 'admin', '127.0.0.1', NULL, '2018-11-28 03:15:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(9, 'admin', '127.0.0.1', NULL, '2018-11-28 21:33:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(10, 'admin', '127.0.0.1', NULL, '2018-11-28 23:06:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(11, 'admin', '127.0.0.1', NULL, '2018-11-29 05:12:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(12, 'admin', '127.0.0.1', NULL, '2018-12-01 23:00:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(13, 'admin', '127.0.0.1', NULL, '2018-12-02 01:15:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(14, 'admin', '127.0.0.1', NULL, '2018-12-02 22:05:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(15, 'admin', '127.0.0.1', NULL, '2018-12-03 05:54:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(16, 'admin', '127.0.0.1', NULL, '2018-12-03 06:00:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(17, 'admin', '127.0.0.1', NULL, '2018-12-03 06:01:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(18, 'admin', '127.0.0.1', NULL, '2018-12-06 01:17:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(19, 'admin', '127.0.0.1', NULL, '2018-12-06 03:26:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(20, 'admin', '192.168.0.111', NULL, '2018-12-08 01:48:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(21, 'admin', '192.168.0.111', NULL, '2018-12-08 01:58:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(22, 'admin', '127.0.0.1', NULL, '2019-01-26 22:53:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(23, 'admin', '127.0.0.1', NULL, '2019-01-27 04:21:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(24, 'admin', '127.0.0.1', NULL, '2019-01-27 22:55:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(25, 'admin', '127.0.0.1', NULL, '2019-01-28 22:41:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(26, 'admin', '127.0.0.1', NULL, '2019-01-29 22:24:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(27, 'admin', '127.0.0.1', NULL, '2019-01-30 02:30:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(28, 'admin', '127.0.0.1', NULL, '2019-01-30 03:52:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(29, 'admin', '127.0.0.1', NULL, '2019-01-30 06:20:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(30, 'admin', '127.0.0.1', NULL, '2019-01-30 06:23:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(31, 'admin', '127.0.0.1', NULL, '2019-01-30 06:26:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(32, 'admin', '127.0.0.1', NULL, '2019-01-30 06:31:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(33, 'admin', '127.0.0.1', NULL, '2019-01-30 06:32:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
(34, 'admin', '127.0.0.1', NULL, '2019-01-31 00:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_11_20_064543_create_table', 2),
(6, '2018_11_22_041044_add_login_fields_to_users_table', 3),
(7, '2018_11_24_062847_create_roles_table', 4),
(8, '2018_11_24_063031_create_role_admins_table', 4),
(9, '2018_12_02_052620_version', 5),
(10, '2018_12_03_053642_server_details', 6),
(11, '2019_01_28_050659_create_article', 7),
(12, '2019_01_31_073245_create_poeams', 8),
(13, '2019_01_31_104151_short_story', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('minhaj@gmail.com', '$2y$10$jPbKdyjSLl.BGi48Vdy2xeILUoCIooGUpCfnER.CVr15fduPaDpEW', '2018-11-19 00:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `poeams`
--

CREATE TABLE `poeams` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poeams_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `status` int(6) NOT NULL DEFAULT '1',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `poeams`
--

INSERT INTO `poeams` (`id`, `author_name`, `title`, `published_date`, `poeams_body`, `date`, `status`, `image`, `created_at`, `updated_at`) VALUES
(4, 'জাকির তালুকদার', 'পদ্মার পলিদ্বীপ : পুরো ব-দ্বীপের মানচিত্র', 'নভেম্বর ১৬, ২০১৮', '<p><span style=\"color:#000000\"><strong>ভাগ্য এক রূপজীবী</strong></span></p>\r\n\r\n<p>ভাগ্য এক রূপজীবী</p>\r\n\r\n<p>সে কখনো থাকে না স্থির;</p>\r\n\r\n<p>কপালে চুলের স্পর্শ দিয়ে</p>\r\n\r\n<p>পালায় সহসা চুমু খেয়ে।</p>\r\n\r\n<p>দুর্ভাগ্য চলে বিপরীতে</p>\r\n\r\n<p>ভালোবেসে কাছে টেনে বলে,</p>\r\n\r\n<p>তার কোনো তাড়া নেই</p>\r\n\r\n<p>বিছানায় বসে থাকে</p>\r\n\r\n<p>তোমার পাশে।</p>\r\n\r\n<p><strong>আহ! আবার সেই চোখগুলো</strong></p>\r\n\r\n<p>আহ! আবার সেই চোখগুলো,</p>\r\n\r\n<p>যা আমায় ভালোবেসে ডাকতো।</p>\r\n\r\n<p>এবং ফের সুমধুর করত</p>\r\n\r\n<p>আমার জীবন।</p>\r\n\r\n<p>সেই কণ্ঠস্বরও এসেছে আবার,</p>\r\n\r\n<p>যা আমি ভালোবেসে শুনতাম।</p>\r\n\r\n<p>শুধু আমি সেই আমি নই,</p>\r\n\r\n<p>রূপান্তরিত হয়ে ফিরেছি ঘরে।</p>\r\n\r\n<p>দুধসাদা সেই বাহুডোর</p>\r\n\r\n<p>আমাকে করতো দৃঢ় আলিঙ্গন,</p>\r\n\r\n<p>এখনো আমি আছি তার হৃদয়ের মাঝে,</p>\r\n\r\n<p>আছি অনুভূতিতে, ভগ্নহৃদয়, নিষ্প্রাণ।</p>\r\n\r\n<p><strong>পুরানো গোলাপ</strong></p>\r\n\r\n<p>একটা গোলাপকুঁড়ি ছিল</p>\r\n\r\n<p>যার জন্য উদ্ভাসিত এ হৃদয়;</p>\r\n\r\n<p>বেড়ে ওঠে সে অপরূপ পুষ্প।</p>\r\n\r\n<p>সে ছিলো আশ্চর্য গোলাপ,</p>\r\n\r\n<p>আর আমি চেয়েছি তাকে ছিন্ন করতে।</p>\r\n\r\n<p>সে জানত, আমার আদরের বিনিময়ে</p>\r\n\r\n<p>কাঁটা বিঁধিয়ে দিতে।</p>\r\n\r\n<p>এখন সে যেখানে ক্ষত করে, ছিন্নভিন্ন করে</p>\r\n\r\n<p>বৃষ্টি আর বাতাসে ভেসে আঘাত করে&mdash;</p>\r\n\r\n<p>প্রিয়তম হাইনরিশ এখন,</p>\r\n\r\n<p>প্রণয়ভরে সে আসে আমার মুখোমুখি।</p>\r\n\r\n<p>হাইনরিশ সামনে, হাইনরিশ পেছনে</p>\r\n\r\n<p>শুনতে যেন সুমধুর শোনায়।</p>\r\n\r\n<p>এই গায়ে বিঁধেছে তোমার কাঁটা,</p>\r\n\r\n<p>এটা কী তোমার ধারালো চিবুক!</p>\r\n\r\n<p>উপরের লোমগুলো বেজায় শক্ত,</p>\r\n\r\n<p>যা তোমার থুতনিকে করে অলংকৃত-</p>\r\n\r\n<p>আশ্রমে যাও, ওহে প্রিয় শিশু</p>\r\n\r\n<p>অথবা মুণ্ডন করবে তোমায়।</p>\r\n\r\n<p><strong>আমি যখন তোমার দিকে তাকাই</strong></p>\r\n\r\n<p>আমি যখন তোমার চোখের দিকে তাকাই</p>\r\n\r\n<p>বিলীন হয়ে যায় আমার যত দুঃখ-কষ্ট;</p>\r\n\r\n<p>আমি যখন তোমার মুখে চুমু খাই,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>আমি তখন হয়ে উঠি পুরোপুরি সুস্থ।</p>\r\n\r\n<p>আমি যখন তোমার বুকে চেঁপে থাকি</p>\r\n\r\n<p>স্বর্গীয় লিপ্সা নামে আমার উপর;</p>\r\n\r\n<p>যখন তুমি বল : আমি তোমায় ভালোবাসি</p>\r\n\r\n<p>মর্মভেদী কান্না আসে আমার।</p>', '2019-01-31 10:15:46', 1, '1548929012.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'client', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_admins`
--

CREATE TABLE `role_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_admins`
--

INSERT INTO `role_admins` (`id`, `role_id`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shortstory`
--

CREATE TABLE `shortstory` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `story_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shortstory`
--

INSERT INTO `shortstory` (`id`, `author_name`, `title`, `published_date`, `story_body`, `date`, `status`, `image`, `created_at`, `updated_at`) VALUES
(2, 'জাকির তালুকদার', 'পদ্মার পলিদ্বীপ : পুরো ব-দ্বীপের মানচিত্র', 'নভেম্বর ১৬, ২০১৮', '<p>প্রাচীন বাড়িটির নিচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নিচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া কাঠের ব্রিজ। দুটি ব্রিজই প্রধান রাস্তার সঙ্গে মিশে গেছে। বের হয়ে যাওয়ার দরোজাটি খোলা পেয়ে একজন সেলিব্রেটি বাড়িতে ঢুকে পড়লেন। তিনি আগে অভিনয় করতেন, বর্তমানে গল্প লেখা ও পরিচালনার সঙ্গেও যুক্ত। তাকে স্বাগত জানালো শ্যামল। সে পেশায় মাছ শিকারী। শ্যামলী ওর বড় বোন। এই প্রাচীন বাড়িটা ওদের। সেলিব্রেটির জন্য অপেক্ষা করতে করতে ঘুমিয়ে গিয়েছিলো। সেলিব্রেটি উপস্থিত হওয়ার কিছু সময় পরও ঘুমাচ্ছিলো শ্যামলী।</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>প্রাচীন বাড়িটির নিচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নিচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া কাঠের ব্রিজ। দুটি ব্রিজই প্রধান রাস্তার সঙ্গে মিশে গেছে। বের হয়ে যাওয়ার দরোজাটি খোলা পেয়ে একজন সেলিব্রেটি বাড়িতে ঢুকে পড়লেন। তিনি আগে অভিনয় করতেন, বর্তমানে গল্প লেখা ও পরিচালনার সঙ্গেও যুক্ত। তাকে স্বাগত জানালো শ্যামল। সে পেশায় মাছ শিকারী। শ্যামলী ওর বড় বোন। এই প্রাচীন বাড়িটা ওদের। সেলিব্রেটির জন্য অপেক্ষা করতে করতে ঘুমিয়ে গিয়েছিলো। সেলিব্রেটি উপস্থিত হওয়ার কিছু সময় পরও ঘুমাচ্ছিলো শ্যামলী।</p>', '2019-01-31 11:35:37', 1, '1548934391.png', NULL, NULL),
(3, 'মীর আরিফ', 'পুলিৎজার বিজয়ী অ্যাডাম জনসনের সাক্ষাৎকার', 'নভেম্বর ১৬, ২০১৮', '<p>প্রাচীন বাড়িটির নিচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নিচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া কাঠের ব্রিজ। দুটি ব্রিজই প্রধান রাস্তার সঙ্গে মিশে গেছে। বের হয়ে যাওয়ার দরোজাটি খোলা পেয়ে একজন সেলিব্রেটি বাড়িতে ঢুকে পড়লেন। তিনি আগে অভিনয় করতেন, বর্তমানে গল্প লেখা ও পরিচালনার সঙ্গেও যুক্ত। তাকে স্বাগত জানালো শ্যামল। সে পেশায় মাছ শিকারী। শ্যামলী ওর বড় বোন। এই প্রাচীন বাড়িটা ওদের। সেলিব্রেটির জন্য অপেক্ষা করতে করতে ঘুমিয়ে গিয়েছিলো। সেলিব্রেটি উপস্থিত হওয়ার কিছু সময় পরও ঘুমাচ্ছিলো শ্যামলী।</p>', '2019-01-31 11:45:43', 1, '1548935143.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `is_admin`, `password`, `remember_token`, `created_at`, `updated_at`, `last_login_at`, `last_login_ip`) VALUES
(19, 'Arif', 'arif@gmail.com', NULL, 1, '$2y$10$bQpxU6BlAnehKI.r0i9Q9O3c3TdS5dAVH1jVhcl6zlr0jmR8HScBe', 'VF21ws7LLMxiu2di24Rt5Th3WUzlSKR5iNSj9B74pGNgEIjCWaNab2yPVdLg', '2018-11-25 00:18:59', '2018-12-08 01:45:06', '2018-12-08 07:45:06', '192.168.0.111'),
(21, 'Tanvir', 'user@gmail.com', NULL, 1, '$2y$10$7qF8WkkU211YJ3LlJWff9u3mk6.7yPKjoxvkgHZmrOA6jSZlOoHRu', 'f5yVDSVMAh3bepoKsfw3rK7xRXpFBoilOv0bJ2AaH3F9Biab9wv9BvUpH2tn', '2018-11-25 00:34:54', '2018-12-08 04:42:56', '2018-12-08 10:42:56', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `id` int(10) UNSIGNED NOT NULL,
  `version_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`id`, `version_name`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', '2018-12-02 08:17:08', 1, NULL, NULL),
(6, '3', '2018-12-03 05:31:13', 1, NULL, NULL),
(7, '4', '2018-12-03 05:31:19', 1, NULL, NULL),
(8, '5', '2018-12-03 05:54:48', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_admin`
--
ALTER TABLE `log_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `poeams`
--
ALTER TABLE `poeams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_admins`
--
ALTER TABLE `role_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shortstory`
--
ALTER TABLE `shortstory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `log_admin`
--
ALTER TABLE `log_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `poeams`
--
ALTER TABLE `poeams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_admins`
--
ALTER TABLE `role_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shortstory`
--
ALTER TABLE `shortstory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
