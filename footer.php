			<!-- BEGIN #footer -->
			<footer id="footer">
				
				<!-- BEGIN .wrapper -->
				<div class="wrapper">
					<div class="call-md-6">
						<img src="images/footerBootomImage.JPG" alt="">
					</div>
					
					<div class="footer-widgets lets-do-4">

						<div class="widget-split item">
							<div class="widget">
								<div>
									<p><a href="index.html"><img class="footerLogo" src="images/logo.png" data-ot-retina="images/logo-footer@2x.png" alt="" /></a></p>
									
								</div>
							</div>
						</div>

						<div class="widget-split item">
							<div class="widget wiggetMobile">
								<h3>অক্ষরে অক্ষরে শিল্প নির্মাণ</h3>
                            </div>
						</div>

						<div class="widget-split item">
							<div class="widget">
								<h3>সম্পাদক : হুমায়ুন কবির </h3>

							</div>
						</div>

						<div class="widget-split item">
							<div class="widget">
								<h3>নির্বাহী সম্পাদক : খালেদ চৌধুরী </h3>
                            </div>
						</div>

					</div>
                    <div class="footerBootom footer-widgets lets-do-4">

                        <div class="widget-split item">
                            <div class="widget">
                                <div>
                                    <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>

                                </div>
                            </div>
                        </div>

                        <div class="widget-split item">
                            <div class="widget">

                                <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>
                            </div>
                        </div>

                        <div class="widget-split item">
                            <div class="widget">

                                <div class="widget-content ot-w-article-list">

                                    <p class="footerText"><i class="fa  fa-envelope"> </i> name.yourdomain@gmail.com</p>
                                    <p class="footerText"><i class="fa  fa-envelope"></i> name.yourdomain@gmail.com</p>

                                </div>
                            </div>
                        </div>

                        <div class="widget-split item">
                            <div class="widget">

                                <p class="footerText">১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা </p>
                            </div>
                        </div>

                    </div>

					<div class="footer-copyright">
						<p class="copyrightText"> বসুধা, রোড ২৮  অ্যাপাটমেন্ট সি-ফোর, গুলশান-১  ঢাকা-১২১২ । মোবাইল : ০১৭৮১-৯০২৭৪৪, ০০০০০০০০০০</p>
					</div>

				<!-- END .wrapper -->
				</div>
				
			<!-- END #footer -->
			</footer>

			<div class="ot-follow-share">
				<a href="#" class="ot-color-facebook" data-h-title="Facebook"><i class="fa fa-facebook"></i></a>
				<a href="#" class="ot-color-twitter" data-h-title="Twitter"><i class="fa fa-twitter"></i></a>
				<a href="#" class="ot-color-google-plus" data-h-title="Google+"><i class="fa fa-google-plus"></i></a>
				<a href="#" class="ot-color-youtube" data-h-title="Youtube"><i class="fa fa-youtube"></i></a>
			</div>

			<div class="ot-responsive-menu-header">
				<a href="#" class="ot-responsive-menu-header-burger"><i class="material-icons">menu</i></a>
				<a href="index.html" class="ot-responsive-menu-header-logo"><img src="images/logo.png" alt="" /></a>
			</div>
			
		<!-- END .boxed -->
		</div>

		<div class="ot-responsive-menu-content-c-header">
			<a href="#" class="ot-responsive-menu-header-burger"><i class="material-icons">menu</i></a>
		</div>
		<div class="ot-responsive-menu-content">
			<div class="ot-responsive-menu-content-inner has-search">
				<form action="http://composs.orange-themes.net/html/blog.html" method="get">
					<input type="text" value="" placeholder="Search" />
					<button type="submit"><i class="fa fa-search"></i></button>
				</form>
				<ul id="responsive-menu-holder"></ul>
			</div>
		</div>
		<div class="ot-responsive-menu-background"></div>

			<!-- Scripts -->
		<script type="text/javascript" src="jscript/jquery-latest.min.js"></script>
		<script type="text/javascript" src="jscript/bootstrap.min.js"></script>
		<script type="text/javascript" src="jscript/theia-sticky-sidebar.min.js"></script>
		<script type="text/javascript" src="jscript/modernizr.js"></script>
		<script type="text/javascript" src="jscript/owl.carousel.min.js"></script>
		<script type="text/javascript" src="jscript/shortcode-scripts.min.js"></script>
		<script type="text/javascript" src="jscript/theme-scripts.min.js"></script>
		<script type="text/javascript" src="jscript/ot-lightbox.min.js"></script>
		
		<script>
			jQuery('.main-slider-owl').owlCarousel({
				margin: 20,
				responsiveClass: true,
				items: 1,
				nav: true,
				dots: false,
				loop: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayHoverPause: true
			});
		</script>

 <script>

</script>
            <script>
                jQuery(window).on('load', function () {
                    jQuery(".page-loader").fadeOut();
                    jQuery(".no-overflow-body").removeClass('no-overflow-body');
                    var d = document, s = d.createElement('script');
                    s.src = 'https://ghungghur.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                });
            </script>

		<!-- Demo Only -->


	<!-- END body -->
	</body>
<!-- END html -->
</html>