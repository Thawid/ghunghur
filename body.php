			<!-- BEGIN .content -->
			<div class="content">

				<!-- BEGIN .wrapper -->
				<div class="wrapper">

					<div class="content-wrapper">

						<!-- BEGIN .composs-main-content -->
						<div class="composs-main-content composs-main-content-s-1">
                            <div class="composs-panel-inner">

                                <div class="composs-main-article-content">

                                    <h1> পুলিৎজার বিজয়ী অ্যাডাম জনসনের সাক্ষাৎকার </h1>

                                    <div class="composs-main-article-head">
                                        <div class="composs-main-article-media">
                                            <img src="images/photos/interview.png" alt="" />
                                        </div>
                                        <div class="composs-main-article-meta">
                                            <span class="item"><i class="fa fa-user"></i> মীর আরিফ   </span>
                                            <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: নভেম্বর ১৬, ২০১৮ </a>
                                        </div>

                                    </div>

                                    <div class="shortcode-content">
                                        <p class="text-justify"><strong>ঢাকা লিটফেস্টে অংশগ্রহণ করে কেমন বোধ করছেন? বাংলাদেশে এটাই কি আপনার প্রথম ভ্রমণ? </strong></p>
                                        <p class="text-justify">হ্যাঁ, এটাই প্রথম বাংলাদেশে আসা। ঢাকা অপূর্ব সুন্দর শহর। আমি এই ধরনের সাহিত্য আসর খুব পছন্দ করি। আমি মনে করি, এই ধরনের সম্প্রদায়কে একসঙ্গে জড়ো করে তাদের বক্তব্যের উপর জোর ও সত্য উচ্চারণে সুযোগ করে দেওয়া একটা বিরাট সম্পদ হয়ে থাকবে। এটার অংশ হতে পেরে আমি গর্বিত।</p>
                                    </div>
                                    <div class="article_bottom">
                                        <a class="more" title="বিস্তারিত" href="interview_details.php"><span>বিস্তারিত</span>:::</a>
                                    </div>
                                </div>

                            </div>

							<!-- BEGIN .composs-panel -->
						<!--	<div class="composs-panel">

								<div class="composs-panel-title composs-panel-title-tabbed">
									<strong class="active">বই নিয়ে</strong>
									<strong>অনুবাদ</strong>
									<strong>পুনর্মুদ্রণ</strong>
								</div>

								<div class="composs-panel-inner">
									<div class="composs-panel-tab active">

										<div class="composs-article-split-block">

											<div class="item-large">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/bookone.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">ঘরণী  -  হুমায়ুন কবির </a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
														<p>১৮৮৩ সালে কাহলিল জিবরান লেবাননের উত্তরে এক গ্রামে জন্মগ্রহণ করেন। তার পিতা কাহলিল; ট্যাক্স কালেক্টর ছিলেন। মায়ের নাম ছিল ক্যামিলা ...</p>
													</div>
												</div>

											</div>

											<div class="item-small">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">‘ত্রিমোহিনী’: ইতিহাস আর গল্পের বাঁকে গড়ে ওঠা এক বর্ণিল ক্যানভাস</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">‘ত্রিমোহিনী’: ইতিহাস আর গল্পের বাঁকে গড়ে ওঠা এক বর্ণিল ক্যানভাস</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">‘ত্রিমোহিনী’: ইতিহাস আর গল্পের বাঁকে গড়ে ওঠা এক বর্ণিল ক্যানভাস</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">‘ত্রিমোহিনী’: ইতিহাস আর গল্পের বাঁকে গড়ে ওঠা এক বর্ণিল ক্যানভাস</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">‘ত্রিমোহিনী’: ইতিহাস আর গল্পের বাঁকে গড়ে ওঠা এক বর্ণিল ক্যানভাস</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

											</div>

										</div>

									</div>
									<div class="composs-panel-tab">

										<div class="composs-article-split-block">
										
										<div class="item-large">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/3.jpg" alt="" /></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">এক জীবনের কথা -  হুমায়ুন কবির </a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
															<a href="javascript:void(0);#comments" class="item-meta-item"><i class="material-icons">chat_bubble_outline</i>35</a>
														</span>
														<p>পৌষের হঠাৎ নামা শীতে গ্রামটা যেন গুটিয়ে গেছে হুট করেই। শীত বেশ হালকা ছিল সপ্তাহখানেক আগেও। বলা নেই কওয়া নেই গত সপ্তাহ থেকে শীত বুড়ি ছানাপোনাসহ হানা দিয়েছে গ্রামে। কুয়াশা জেঁকে বসছে সূর্য পশ্চিমে হেলার সাথে সাথেই। যে কারণে গ্রামের ছেলে.. ...</p>
													</div>
												</div>

											</div>

											<div class="item-small">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="composs-panel-tab">

										<div class="composs-article-split-block">
										
										<div class="item-large">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/4.jpg" alt="" /></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">এক জীবনের কথা -  হুমায়ুন কবির </a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
															<a href="javascript:void(0);#comments" class="item-meta-item"><i class="material-icons">chat_bubble_outline</i>35</a>
														</span>
														<p>পৌষের হঠাৎ নামা শীতে গ্রামটা যেন গুটিয়ে গেছে হুট করেই। শীত বেশ হালকা ছিল সপ্তাহখানেক আগেও। বলা নেই কওয়া নেই গত সপ্তাহ থেকে শীত বুড়ি ছানাপোনাসহ হানা দিয়েছে গ্রামে। কুয়াশা জেঁকে বসছে সূর্য পশ্চিমে হেলার সাথে সাথেই। যে কারণে গ্রামের ছেলে.. ...</p>
													</div>
												</div>

											</div>

											<div class="item-small">

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>

												<div class="item">
													<div class="item-header">
														
														<a href="javascript:void(0);"><img src="images/photos/2.jpg" alt=""/></a>
													</div>
													<div class="item-content">
														<h2><a href="javascript:void(0);">হিন্দু-শিখ দ্বন্দ্ব নিয়ে : ক্যাথি অসলারের কাব্যিক উপন্যাস ‘কর্ম’</a></h2>
														<span class="item-meta">
															<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2018</span>
														</span>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>


							</div>-->

							<!-- BEGIN .composs-panel -->
							

							<!-- BEGIN .composs-panel -->
							<div class="composs-panel">

								<div class="composs-panel-title">
									<strong>সাম্প্রতিক  নিবন্ধন </strong>
								</div>

								<div class="composs-panel-inner">

									<div class="composs-blog-list lets-do-1">

										<div class="item">
											<div class="item-header">
												
												<a href="javascript:void(0);"><img src="images/photos/image-11.jpg" alt="" /></a>
											</div>
											<div class="item-content">
												<h2><a href="javascript:void(0);">ভারতীয় সিনেমায় বাংলাদেশের টেররিস্ট রিপ্রেজেন্টেশন এবং আমাদের ‘সিনেমা সংস্কৃতি’র সংকট</a></h2>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
												</span>
												<p>সিনেমা মূলত পারভার্ট আর্ট। ইহা আপনাকে আপনার বাসনা মোতাবেক কিছু দেয় না—ইহা আপনার “বাসনা”কে ঠিক করিয়া দেয়।’ —স্লাভো জিজেক [২০০৯]...</p>
											</div>
										</div>

										<div class="item">
											<div class="item-header">
												
												<a href="javascript:void(0);"><img src="images/photos/image-11.jpg" alt="" /></a>
											</div>
											<div class="item-content">
												<h2><a href="javascript:void(0);">ভারতীয় সিনেমায় বাংলাদেশের টেররিস্ট রিপ্রেজেন্টেশন এবং আমাদের ‘সিনেমা সংস্কৃতি’র সংকট</a></h2>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
												</span>
												<p>সিনেমা মূলত পারভার্ট আর্ট। ইহা আপনাকে আপনার বাসনা মোতাবেক কিছু দেয় না—ইহা আপনার “বাসনা”কে ঠিক করিয়া দেয়।’ —স্লাভো জিজেক [২০০৯]...</p>
											</div>
										</div>

										

										
										<div class="item">
											<div class="item-header">
												
												<a href="javascript:void(0);"><img src="images/photos/image-11.jpg" alt="" /></a>
											</div>
											<div class="item-content">
												<h2><a href="javascript:void(0);">ভারতীয় সিনেমায় বাংলাদেশের টেররিস্ট রিপ্রেজেন্টেশন এবং আমাদের ‘সিনেমা সংস্কৃতি’র সংকট</a></h2>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
												</span>
												<p>সিনেমা মূলত পারভার্ট আর্ট। ইহা আপনাকে আপনার বাসনা মোতাবেক কিছু দেয় না—ইহা আপনার “বাসনা”কে ঠিক করিয়া দেয়।’ —স্লাভো জিজেক [২০০৯]...</p>
											</div>
										</div>

										<div class="item">
											<div class="item-header">
												
												<a href="javascript:void(0);"><img src="images/photos/image-11.jpg" alt="" /></a>
											</div>
											<div class="item-content">
												<h2><a href="javascript:void(0);">ভারতীয় সিনেমায় বাংলাদেশের টেররিস্ট রিপ্রেজেন্টেশন এবং আমাদের ‘সিনেমা সংস্কৃতি’র সংকট</a></h2>
												<span class="item-meta">
													<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
												</span>
												<p>সিনেমা মূলত পারভার্ট আর্ট। ইহা আপনাকে আপনার বাসনা মোতাবেক কিছু দেয় না—ইহা আপনার “বাসনা”কে ঠিক করিয়া দেয়।’ —স্লাভো জিজেক [২০০৯]...</p>
											</div>
										</div>

									</div>

								</div>

								<!---<div class="composs-panel-pager">
									<a class="prev page-numbers" href="#"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Previous</a>
									<a class="page-numbers" href="#">1</a>
									<span class="page-numbers current">2</span>
									<a class="page-numbers" href="#">3</a>
									<a class="page-numbers" href="#">4</a>
									<a class="page-numbers" href="#">5</a>
									<a class="next page-numbers" href="#">Next <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
								</div>  ---->

								<div class="composs-panel-pager">
									<a href="javascript:void(0);" class="composs-pager-button">View more articles</a>
								</div>

								

							<!-- END .composs-panel -->
							</div>

						<!-- END .composs-main-content -->
						</div>
						
            <?php include_once 'home_sidebar.php';  ?>
			