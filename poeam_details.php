
<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

<!-- BEGIN .content -->
<div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <div class="content-wrapper">

            <!-- BEGIN .composs-main-content -->
            <div class="composs-main-content composs-main-content-s-1">

                <div class="theiaStickySidebar">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <!-- <div class="composs-panel-title">
                            <strong>Blog page style #1</strong>
                        </div> -->

                        <div class="composs-panel-inner">

                            <div class="composs-main-article-content">

                                <h1>হাইনরিশ হাইনের কবিতা</h1>

                                <div class="composs-main-article-head">
                                    <div class="composs-main-article-media">
                                        <img src="images/photos/poeam.png" alt="" />
                                    </div>
                                    <div class="composs-main-article-meta">
                                        <span class="item"><i class="fa fa-user"></i>হেমায়েত মাতুব্বর</span>
                                        <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: ডিসেম্বর ০৮, ২০১৮</a>
                                    </div>

                                </div>

                                <div class="shortcode-content">
                                    <p> <strong> ভাগ্য এক রূপজীবী </strong> </p>

                                    <p>    ভাগ্য এক রূপজীবী</p>

                                    <p>    সে কখনো থাকে না স্থির;</p>

                                    <p>    কপালে চুলের স্পর্শ দিয়ে</p>

                                    <p>     পালায় সহসা চুমু খেয়ে।</p>



                                    <p>    দুর্ভাগ্য চলে বিপরীতে</p>

                                    <p>    ভালোবেসে কাছে টেনে বলে,</p>

                                    <p>     তার কোনো তাড়া নেই</p>

                                    <p>   বিছানায় বসে থাকে</p>

                                    <p>    তোমার পাশে।</p>



                                    <p> <strong>  আহ! আবার সেই চোখগুলো </strong> </p>

                                    <p>   আহ! আবার সেই চোখগুলো,</p>

                                    <p>    যা আমায় ভালোবেসে ডাকতো।</p>

                                    <p>    এবং ফের সুমধুর করত</p>

                                    <p>    আমার জীবন।</p>



                                    <p>    সেই কণ্ঠস্বরও এসেছে আবার,</p>

                                    <p>     যা আমি ভালোবেসে শুনতাম।</p>

                                    <p>   শুধু আমি সেই আমি নই,</p>

                                    <p>   রূপান্তরিত হয়ে ফিরেছি ঘরে।</p>



                                    <p>     দুধসাদা সেই বাহুডোর</p>

                                    <p>    আমাকে করতো দৃঢ় আলিঙ্গন,</p>

                                    <p>    এখনো আমি আছি তার হৃদয়ের মাঝে,</p>

                                    <p>   আছি অনুভূতিতে, ভগ্নহৃদয়, নিষ্প্রাণ।</p>



                                    <p>  <strong>  পুরানো গোলাপ </strong> </p>

                                    <p>    একটা গোলাপকুঁড়ি ছিল</p>

                                    <p>    যার জন্য উদ্ভাসিত এ হৃদয়;</p>

                                    <p>    বেড়ে ওঠে সে অপরূপ পুষ্প।</p>



                                    <p>   সে ছিলো আশ্চর্য গোলাপ,</p>

                                    <p>   আর আমি চেয়েছি তাকে ছিন্ন করতে।</p>

                                    <p>    সে জানত, আমার আদরের বিনিময়ে</p>

                                    <p>   কাঁটা বিঁধিয়ে দিতে।</p>

                                    <p>    এখন সে যেখানে ক্ষত করে, ছিন্নভিন্ন করে</p>

                                    <p>   বৃষ্টি আর বাতাসে ভেসে আঘাত করে—</p>

                                    <p>   প্রিয়তম হাইনরিশ এখন,</p>

                                    <p>   প্রণয়ভরে সে আসে আমার মুখোমুখি।</p>



                                    <p>    হাইনরিশ সামনে, হাইনরিশ পেছনে</p>

                                    <p>    শুনতে যেন সুমধুর শোনায়।</p>

                                    <p>   এই গায়ে বিঁধেছে তোমার কাঁটা,</p>

                                    <p>   এটা কী তোমার ধারালো চিবুক!</p>



                                    <p>    উপরের লোমগুলো বেজায় শক্ত,</p>

                                    <p>    যা তোমার থুতনিকে করে অলংকৃত-</p>

                                    <p>    আশ্রমে যাও, ওহে প্রিয় শিশু</p>

                                    <p>   অথবা মুণ্ডন করবে তোমায়।</p>



                                    <p> <strong>  আমি যখন তোমার দিকে তাকাই </strong> </p>

                                    <p>   আমি যখন তোমার চোখের দিকে তাকাই</p>

                                    <p>    বিলীন হয়ে যায় আমার যত দুঃখ-কষ্ট;</p>

                                         আমি যখন তোমার মুখে চুমু খাই,</p>

                                    <p>   আমি তখন হয়ে উঠি পুরোপুরি সুস্থ।</p>



                                    <p>    আমি যখন তোমার বুকে চেঁপে থাকি</p>

                                    <p>    স্বর্গীয় লিপ্সা নামে আমার উপর;</p>

                                    <p>   যখন তুমি বল : আমি তোমায় ভালোবাসি</p>

                                    <p>    মর্মভেদী কান্না আসে আমার। </p>
                                </div>
                            </div>

                        </div>

                        <!-- END .composs-panel -->
                    </div>

                </div>

                <!-- END .composs-main-content -->
            </div>

            <!-- BEGIN #sidebar -->
            <?php include_once 'sidebar.php';  ?>


<?php include_once ('footer.php'); ?>