
<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

<!-- BEGIN .content -->
<div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <div class="content-wrapper">

            <!-- BEGIN .composs-main-content -->
            <div class="composs-main-content composs-main-content-s-1">

                <div class="theiaStickySidebar">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <!-- <div class="composs-panel-title">
                            <strong>Blog page style #1</strong>
                        </div> -->

                        <div class="composs-panel-inner">

                            <div class="composs-main-article-content">

                                <h1> পুলিৎজার বিজয়ী অ্যাডাম জনসনের সাক্ষাৎকার </h1>

                                <div class="composs-main-article-head">
                                    <div class="composs-main-article-media">
                                        <img src="images/photos/interview.png" alt=""/>
                                    </div>
                                    <div class="composs-main-article-meta">
                                        <span class="item"><i class="fa fa-user"></i> মীর আরিফ   </span>
                                        <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: নভেম্বর ১৬, ২০১৮ </a>
                                    </div>

                                </div>

                                <div class="shortcode-content">
                                    <p class="text-justify"><strong>ঢাকা লিটফেস্টে অংশগ্রহণ করে কেমন বোধ করছেন? বাংলাদেশে এটাই কি আপনার প্রথম ভ্রমণ? </strong></p>
                                    <p class="text-justify">হ্যাঁ, এটাই প্রথম বাংলাদেশে আসা। ঢাকা অপূর্ব সুন্দর শহর। আমি এই ধরনের সাহিত্য আসর খুব পছন্দ করি। আমি মনে করি, এই ধরনের সম্প্রদায়কে একসঙ্গে জড়ো করে তাদের বক্তব্যের উপর জোর ও সত্য উচ্চারণে সুযোগ করে দেওয়া একটা বিরাট সম্পদ হয়ে থাকবে। এটার অংশ হতে পেরে আমি গর্বিত।</p>
                                    <p class="text-justify"><strong>এবার আপনার উপন্যাস ‘দি অরফ্যান মাস্টার’স সন’ এর প্রসঙ্গে আসি। উত্তর কোরিয়া এটার বিষয়বস্তু। এক নির্জনবাসী রাজ্যের ঝামেলাপূর্ণ দিকের কথা গভীরভাবে বিশ্লেষণ করা হয়েছে এখানে। উপন্যাসটি লিখতে কোন প্রেরণা আপনাকে উৎসাহিত করেছে? </strong></p>
                                    <p class="text-justify"> আচ্ছা। এখানে বলবো যে, আমি শুরু করেছিলাম পাঠক হিসেবে। পৃথিবীর সবচেয়ে নিশ্চুপ ও উৎপীড়িত এই দেশটি নিয়ে আমি বরাবরই কৌতূহলি ছিলাম। হতে পারে পৃথিবীর অনেক জায়গা শারীরিক, আবেগীয় ও মনস্তাত্ত্বিকভাবে ভয়ংকর অবস্থানে আছে কিন্তু আমি বলবো উত্তর কোরিয়া তাদের পূর্ণ অস্তিত্বের প্রশ্নে মানবিকতার দিক থেকে সবচেয়ে খারাপ অবস্থানে রয়েছে। কিন্তু পাঠক হিসেবে আমি আবিষ্কার করলাম যে, উত্তর কোরিয়াতে মানুষের পূর্ণ প্রতিকৃতি খুঁজে পাওয়া দুষ্কর, কারণ সেখানে বই লেখা অনুমোদিত নয়। উত্তর কোরিয়া থেকে পালিয়ে থাকা মানুষগুলোও তৎক্ষণাৎ তাদের গল্প বলতে প্রস্তুত নয়। সুতরাং আমি এক বিস্ময় নিয়ে তাদের পক্ষে আমার কণ্ঠে আওয়াজ তুললাম।</p>
                                </div>
                            </div>

                        </div>

                        <!-- END .composs-panel -->
                    </div>

                </div>

                <!-- END .composs-main-content -->
            </div>

            <!-- BEGIN #sidebar -->
            <?php include_once 'sidebar.php';  ?>

<?php include_once ('footer.php'); ?>