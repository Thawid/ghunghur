
<body class="ot-menu-will-follow">
<div class="page-loader">
   
	<div class="spinner"></div>
</div>

		<!-- BEGIN .boxed -->
		<div class="boxed">
			
			<!-- BEGIN .header -->
			<div class="header">
				
				<!-- BEGIN .wrapper -->
				<div class="wrapper">

					<nav class="header-top">
						<div class="header-top-socials">
							<a href="#" class="ot-color-hover-facebook"><i class="fa fa-facebook"></i></a>
							<a href="#" class="ot-color-hover-twitter"><i class="fa fa-twitter"></i></a>
							<a href="#" class="ot-color-hover-google-plus"><i class="fa fa-google-plus"></i></a>
							<a href="#" class="ot-color-hover-youtube"><i class="fa fa-youtube"></i></a>
						</div>
						<ul>
							<li><a href="mailto:info@yourdomain.com"> <i class="fa fa-envelope"></i> info@yourdomain.com</a></li>
							<li><a href="tel:88018000000"><i class="fa fa-phone-square"></i> +88018000000</a></li>

						</ul>
					</nav>

					<div class="header-content">

						<div class="header-content-logo">
							<a href="index.php"><img class="headerlogo" src="images/logo.png" data-ot-retina="images/logo@2x.png" alt="" /></a>
						</div>

						<div class="header-content-o">
							<a href="#" target="_blank"><img src="images/Webp.net-gifmaker.gif" alt="" /></a>
						</div>

					</div>

					<div class="main-menu-placeholder wrapper">
						<nav id="main-menu">
							<ul>
								<li><a href="index.php"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a></li>
								<li><a href="artical.php">প্রবন্ধ / নিবন্ধ </a></li>
								<li><a href="poeams.php">কবিতা </a></li>
								<li><a href="short_stories.php">ছোট গল্প </a></li>
								<li><a href="series.php">ধারাবাহিক</a></li>
								<li><a href="interview.php"> সাক্ষাৎকার </a></li>
								
								<li><a href="painting.php">চিত্রকলা    </a></li>
								
								<li><a href="movies.php">চলচ্চিত্র    </a></li>
								<li><a href="drama.php">নাটক    </a></li>
								<li><a href="rendering.php">অনুবাদ    </a></li>
								<li><a href="special_edition.php">বিশেষ সংখ্যা     </a></li>
								<li><a href="javascript:void(0);">পুনর্পাঠ    </a></li>
								<li><a href="book_house.php"> বই ঘর    </a></li>
								<li><a href="archive.php"> ই-ঘুংঘুর </a></li>
								<li><a href="rhyme.php"> বিবিধ   </a></li>
								<li class="menuLastTab"><a href="contact.php">যোগাযোগ </a></li>
							</ul>
							
						</nav>
					</div>

				<!-- END .wrapper -->
				</div>
				
			<!-- END .header -->
			</div>