
<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

<!-- BEGIN .content -->
<div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <div class="content-wrapper">

            <!-- BEGIN .composs-main-content -->
            <div class="composs-main-content composs-main-content-s-1">

                <div class="theiaStickySidebar">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <!-- <div class="composs-panel-title">
                            <strong>Blog page style #1</strong>
                        </div> -->

                        <div class="composs-panel-inner">

                            <div class="composs-main-article-content">

                                <h1>সেলিব্রেটি</h1>

                                <div class="composs-main-article-head">
                                    <div class="composs-main-article-media">
                                        <img src="images/photos/short_stories.png" alt="" />
                                    </div>
                                    <div class="composs-main-article-meta">
                                        <span class="item"><i class="fa fa-user"></i>স্বরলিপি </span>
                                        <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: জানুয়ারি ০৮, ২০১৯</a>
                                    </div>

                                </div>

                                <div class="shortcode-content">
                                    <p class="text-justify"> প্রাচীন বাড়িটির নিচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নিচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া কাঠের ব্রিজ। দুটি ব্রিজই প্রধান রাস্তার সঙ্গে মিশে গেছে। বের হয়ে যাওয়ার দরোজাটি খোলা পেয়ে একজন সেলিব্রেটি বাড়িতে ঢুকে পড়লেন। তিনি আগে অভিনয় করতেন, বর্তমানে গল্প লেখা ও পরিচালনার সঙ্গেও যুক্ত। তাকে স্বাগত জানালো শ্যামল। সে পেশায় মাছ শিকারী। শ্যামলী ওর বড় বোন। এই প্রাচীন বাড়িটা ওদের। সেলিব্রেটির জন্য অপেক্ষা করতে করতে ঘুমিয়ে গিয়েছিলো। সেলিব্রেটি উপস্থিত হওয়ার কিছু সময় পরও ঘুমাচ্ছিলো শ্যামলী। </p>
                                    <p class="text-justify"> —আপনার মুখ দেখে মনে হচ্ছে, আমার ঘুমিয়ে যাওয়াটা ঠিক হয়নি। সত্যিই আমি দুঃখিত। আপনাকে বুঝি স্বাগত জানিয়েছে আমার ভাই?  আপনি বললেন,  যে দরোজা খুলে দিয়েছে সে কে? আমি মূলত তার কথায় বলছিলাম। আপনি রেগে আছেন সে আমি বুঝতে পারছি। ঠিক করে উঠতে পারছি না আপনাকে কোথায় বসতে দেবো।</p>
                                    <p class="text-justify"> আমি থাকি দ্বিতীয় তলায় আর আমার ভাই থাকে তৃতীয় তলায়। আপনি শুটিংয়ের লোকেশন হিসেবে বাড়িটি উপযোগী কিনা, তাই দেখতে এসেছেন। এই সুবাদে আপনাকে কাছ থেকে দেখা হল। জানেন, বড় পোস্টারে আপনার মায়াভরা মুখ আমি দেখেছি সেই ছোটবেলায়। স্কুলে যেতাম। মাথায় ওড়না টেনে তারপর পোস্টারের দিকে তাকিয়ে থাকতাম, যাতে মানুষ আমাকে দেখতে না পারে। বোঝেনতো পোস্টারের দিকে তাকিয়ে থাকাটা খুব একটা ভালো চোখে দেখা হতো না। আপনাকে দেখতাম আর মনে হতো, মুখে গভীর ছায়া পড়ে আছে।</p>
                                    <p class="text-justify"> এতো কথা বলছি শুনে আপনি ভাববেন না অসম্মান করছি। তবু আপনাকে বলার লোভ সামলাতে পারছি না। আজ যখন গোসল করছিলাম তখনও মনে মনে ঈশ্বরকে বলছিলাম, আপনার সঙ্গে যেন দেখা হয়ে যায়। এখন মনে প্রশ্ন জাগছে ওই অবস্থায় ঈশ্বরের সঙ্গে কথা বলেও কি ভুল করে ফেললাম। একথা বলার জন্য আমার হয়তো আর একটু আড়াল নেওয়া দরকার ছিল।</p>
                                </div>
                            </div>

                        </div>

                        <!-- END .composs-panel -->
                    </div>

                </div>

                <!-- END .composs-main-content -->
            </div>

            <!-- BEGIN #sidebar -->
            <?php include_once 'sidebar.php';  ?>

<?php include_once ('footer.php'); ?>