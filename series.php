<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

    <!-- BEGIN .content -->
    <div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

    <div class="content-wrapper">

    <!-- BEGIN .composs-main-content -->
    <div class="composs-main-content composs-main-content-s-1">

        <!-- BEGIN .composs-panel -->
        <div class="composs-panel">

            <div class="composs-panel-title">
                <strong>হোম <i class="fa fa-chevron-right"></i> ধারাবাহিক </strong>
            </div>

            <div class="composs-panel-inner">

                <div class="composs-blog-list lets-do-3">

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>ড. রাশিদা ইকবালের এই কঠিন শ্রমলব্ধ গবেষণা এবং হোটেল গুরু ইন্টারন্যাশনালের বাঙালি বাবুর্চিদের রান্না আর বাঙালি মেসওয়েটারদের আন্তরিক আপ্যায়ন থেকে...</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="series_details.php"><img src="images/photos/series.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>মুহম্মদ মুহসিন </span>

										</span>
                            <h2><a href="series_details.php"> কালাপানির কেচ্ছা ।। পর্ব ০৭ </a></h2>
                            <span class="item-meta">
													<a href="series_details.php" class="item-meta-item"><i class="material-icons">access_time</i>জানুয়ারি ১৭, ২০১৯ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="series_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="composs-panel-pager">
                <a class="prev page-numbers" href="#"><i class="fa fa-angle-double-left"></i>Previous</a>
                <a class="page-numbers" href="#">1</a>
                <span class="page-numbers current">2</span>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">4</a>
                <a class="page-numbers" href="#">5</a>
                <a class="next page-numbers" href="#">Next<i class="fa fa-angle-double-right"></i></a>
            </div>
            <!-- END .composs-panel -->
        </div>

        <!-- END .composs-main-content -->
    </div>

<?php include_once 'sidebar.php';  ?>

<?php include_once ('footer.php'); ?>