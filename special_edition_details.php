
<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

<!-- BEGIN .content -->
<div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <div class="content-wrapper">

            <!-- BEGIN .composs-main-content -->
            <div class="composs-main-content composs-main-content-s-1">

                <div class="theiaStickySidebar">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <!-- <div class="composs-panel-title">
                            <strong>Blog page style #1</strong>
                        </div> -->

                        <div class="composs-panel-inner">

                            <div class="composs-main-article-content">

                                <h1> বিজয়ের স্মৃতি ।। যতীন সরকার </h1>

                                <div class="composs-main-article-head">
                                    <div class="composs-main-article-media">
                                        <img src="images/photos/special_edition.png" alt=""/>
                                    </div>
                                    <div class="composs-main-article-meta">
                                        <span class="item"><i class="fa fa-user"></i> যতীন সরকার  </span>
                                        <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত: ডিসেম্বর ১৬, ২০১৮ </a>
                                    </div>

                                </div>

                                <div class="shortcode-content">
                                    <p class="text-justify">১৯৭১ সালের ১৬ মার্চ থেকে আমি ময়মনসিংহ নাসীরাবাদ কলেজে অধ্যাপনা শুরু করি। সেই মার্চের নয় কী দশ তারিখের ঘটনা—ময়মনসিংহ শহরের সবচেয়ে ব্যস্ত মেয়েদের হাইস্কুল মহাখালী পাঠশালা—সেই স্কুলসহ কোনো প্রতিষ্ঠানেই ক্লাস হচ্ছিল না। চলছে অসহযোগ আন্দোলন। কোনো মেয়েদের স্কুলে ছেলেদের ঢুকে পড়ার কথা নয়। স্কুল থেকে কিছু ছেলেমেয়ে হইহই করতে করতে মহাখালী পাঠশালা থেকে মিছিল করতে করতে বের হয়ে আসছে। সামনের ছেলেটির হাতেই একটি ছবি। রাস্তায় ছেলেটি নিজেই ছবিটি উপরে তুলে ধরলো। সবাই দেখলাম ছবিটি জিন্নাহর। মূহূর্তের মধ্যে উত্তেজিত উল্লাসিত ছেলেরা ছবিটি টুকরো টুকরো করে ছিঁড়ে ফেললো। টুকরোগুলো রাস্তায় ছড়িয়ে পা দিয়ে মাড়াতে লাগলো। পরে শুনেছি শহরে যত স্কুল কলেজ অফিস আদালতে জিন্নাহর যত ছবি ছিল সবগুলিরই একই পরিণতি ঘটেছে। তখন বুঝতে আর বাকি রইলো না যে, মুমূর্ষু পাকিস্তান রাষ্ট্রটিকে অক্সিজেন দিয়েও আর বাঁচিয়ে রাখার উপায় নেই। মুক্তিযুদ্ধ শুরু হওয়ার পরেই আমি মেঘালয়ে চলে গিয়েছিলাম।</p>
                                </div>
                                <div class="ot-shortcode-paragraph-row">
                                    <div class="column8">

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-hand-o-right"></i><a href="javascript:void(0);">বিজয়ের স্মৃতি ।। যতীন সরকার </a> </li>
                                            <li><i class="fa-li fa fa-hand-o-right"></i><a href="javascript:void(0);">বিজয়ের স্মৃতি ।। যতীন সরকার </a></li>
                                            <li><i class="fa-li fa fa-hand-o-right"></i><a href="javascript:void(0);">বিজয়ের স্মৃতি ।। যতীন সরকার </a></li>
                                            <li><i class="fa-li fa fa-hand-o-right"></i><a href="javascript:void(0);">বিজয়ের স্মৃতি ।। যতীন সরকার </a></li>
                                        </ul>
                                    </div>


                                </div>
                            </div>

                        </div>

                        <!-- END .composs-panel -->
                    </div>

                </div>

                <!-- END .composs-main-content -->
            </div>

            <!-- BEGIN #sidebar -->
            <?php include_once 'sidebar.php';  ?>


<?php include_once ('footer.php'); ?>