<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

    <!-- BEGIN .content -->
    <div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

    <div class="content-wrapper">

    <!-- BEGIN .composs-main-content -->
    <div class="composs-main-content composs-main-content-s-1">

        <!-- BEGIN .composs-panel -->
        <div class="composs-panel">

            <div class="composs-panel-title">
                <strong>হোম <i class="fa fa-chevron-right"></i> অনুবাদ  </strong>
            </div>

            <div class="composs-panel-inner">

                <div class="composs-blog-list lets-do-3">

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php"> বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>১৯৭১ সালের ১৬ মার্চ থেকে আমি ময়মনসিংহ নাসীরাবাদ কলেজে অধ্যাপনা শুরু করি। সেই মার্চের নয় কী দশ তারিখের ঘটনা—ময়মনসিংহ শহরের সবচেয়ে ব্যস্ত...</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item-header">

                            <a href="rendering_details.php"><img src="images/photos/special_edition.png" alt="" /></a>
                        </div>
                        <div class="item-content">
                                        <span class="item-meta">
													<span class="item-meta-item"><i class="fa fa-user"></i>  যতীন সরকার </span>

										</span>
                            <h2><a href="rendering_details.php">
                                    বিজয়ের স্মৃতি ।। যতীন সরকার </a></h2>
                            <span class="item-meta">
													<a href="rendering_details.php" class="item-meta-item"><i class="material-icons">access_time</i> ডিসেম্বর ১৬, ২০১৮ </a>
										</span>
                            <p>প্রাচীন বাড়িটির নীচতলা ডুবে গেছে। দ্বিতীয় তলাকেই নীচতলা মনে হয়। বাড়িতে ঢোকার দুটি দরোজা। একটি প্রবেশের অন্যটি বের হয়ে যাওয়ার জন্য। দরোজায় লাগোয়া...২০১৯</p>
                            <div class="article_bottom">
                                <a class="more" title="বিস্তারিত" href="rendering_details.php"><span>বিস্তারিত</span>:::</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="composs-panel-pager">
                <a class="prev page-numbers" href="#"><i class="fa fa-angle-double-left"></i>Previous</a>
                <a class="page-numbers" href="#">1</a>
                <span class="page-numbers current">2</span>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">4</a>
                <a class="page-numbers" href="#">5</a>
                <a class="next page-numbers" href="#">Next<i class="fa fa-angle-double-right"></i></a>
            </div>
            <!-- END .composs-panel -->
        </div>

        <!-- END .composs-main-content -->
    </div>

<?php include_once 'sidebar.php';  ?>

<?php include_once ('footer.php'); ?>