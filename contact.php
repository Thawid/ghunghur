<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

    <!-- BEGIN .content -->
    <div class="content">

        <!-- BEGIN .wrapper -->
        <div class="wrapper">

            <div class="content-wrapper">

                <!-- BEGIN .composs-main-content -->
                <div class="composs-main-content composs-main-content-s-1">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <div class="composs-panel-title">
                            <strong>Come visit us</strong>
                        </div>

                        <div class="composs-panel-inner">
                            <div class="map-block">
                                <div class="map-block-header">
                                    <h3>Composs office</h3>
                                    <div class="paragraph-row">
                                        <div class="column4">
                                            <p>Eos in minim argumentum accommodare. Cu augue mentitum copiosae quo, est ad assum aliquam. Dictas tractatos adipiscing ex mei.</p>
                                        </div>
                                        <div class="column4">
                                            <ul>
                                                <li><i class="material-icons">phone</i>+44 28 9045 7599</li>
                                                <li><i class="material-icons">location_on</i>375 Upper Newtownards Rd, Belfast, UK BT5 5DL</li>
                                                <li><a href="#"><i class="material-icons">email</i>info@milka.com</a></li>
                                            </ul>
                                        </div>
                                        <div class="column4">
                                            <i class="material-icons large-icon">location_city</i>
                                            <!-- <i class="material-icons large-icon">location_on</i> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- END .composs-panel -->
                    </div>

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <div class="composs-panel-title">
                            <strong>Contact us</strong>
                        </div>

                        <div class="composs-panel-inner">
                            <div class="comment-form">
                                <div id="respond" class="comment-respond">

                                    <form action="#" class="comment-form">
                                        <!--<div class="alert-message ot-shortcode-alert-message alert-green">
                                            <strong>Success! This a success message</strong>
                                        </div>  --->
                                        <!-- <div class="alert-message ot-shortcode-alert-message alert-red">
                                            <strong>Error! This an error message</strong>
                                        </div>
                                        <div class="alert-message ot-shortcode-alert-message">
                                            <strong>Warning! This a warning message</strong>
                                        </div> -->
                                        <div class="contact-form-content">
                                            <p class="contact-form-user">
                                                <label class="label-input">
                                                    <span>Nickname<i class="required">*</i></span>
                                                    <input type="text" placeholder="Nickname" name="nickname" value="">
                                                </label>
                                            </p>
                                            <p class="contact-form-email">
                                                <label class="label-input">
                                                    <span>E-mail<i class="required">*</i></span>
                                                    <input type="email" placeholder="E-mail" name="email" value="">
                                                </label>
                                            </p>
                                            <p class="contact-form-comment">
                                                <label class="label-input">
                                                    <span>Message text<i class="required">*</i></span>
                                                    <textarea name="comment" placeholder="Message text"></textarea>
                                                </label>
                                            </p>
                                            <p class="form-submit">
                                                <input name="submit" type="submit" id="submit" class="submit button" value="Send this message">
                                            </p>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                        <!-- END .composs-panel -->
                    </div>

                    <!-- END .composs-main-content -->
                </div>
<?php include_once 'sidebar.php';  ?>
<?php include_once ('footer.php'); ?>