
<?php include_once ('header.php'); ?>
<?php include_once ('header_menu.php'); ?>

<!-- BEGIN .content -->
<div class="content">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <div class="content-wrapper">

            <!-- BEGIN .composs-main-content -->
            <div class="composs-main-content composs-main-content-s-1">

                <div class="theiaStickySidebar">

                    <!-- BEGIN .composs-panel -->
                    <div class="composs-panel">

                        <!-- <div class="composs-panel-title">
                            <strong>Blog page style #1</strong>
                        </div> -->

                        <div class="composs-panel-inner">

                            <div class="composs-main-article-content">

                                <h1>পদ্মার পলিদ্বীপ : পুরো ব-দ্বীপের মানচিত্র</h1>

                                <div class="composs-main-article-head">
                                    <div class="composs-main-article-media">
                                        <img src="images/photos/artical.png" alt="" />
                                    </div>
                                    <div class="composs-main-article-meta">
                                        <span class="item"><i class="fa fa-user"></i>জাকির তালুকদার</span>
                                        <a href="#comments" class="item"><i class="material-icons">access_time</i>প্রকাশিত:  জানুয়ারি ১১, ২০১৯</a>
                                    </div>

                                </div>

                                <div class="shortcode-content">
                                    <p class="text-justify">সন্ধ্যা ঘনিয়ে এসেছিলো সেদিনও। সশস্ত্র পুলিশের কব্জায় থাকা নিরাপত্তার এক কঠিন গোপনীয় আবহে কলকাতার এসএসকেএম মেডিকেল কলেজ থেকে কেওড়াতলা শ্মশানে নিয়ে আসা হচ্ছে এক প্রাণহীন দেহ। মনে হচ্ছিলো যেন কেউ শহরে কার্ফু জারি করেছে। অনেকক্ষণ অপেক্ষার পর নিষ্প্রাণ দেহটি পৌঁছুল শ্মশানে।</p>
                                    <p class="text-justify">সেখানেও প্যারামিলিটারি আর অস্ত্রে সজ্জিত পুলিশের প্রহরা। এক নারী, দুজন মেয়ে এবং একটি ছেলে – শোকে কাতর একই পরিবারের চারজন মানুষ এই পুলিশের জঙ্গলের মাঝে একা দাঁড়িয়ে রয়েছেন। এক পুলিশ ছেলেটির হাতে মশাল তুলে দিলো তার বাবার চিতায় আগুন ধরাবার জন্যে।</p>
                                    <p class="text-justify">এভাবেই বিপ্লবী চারু মজুমদারের নশ্বর দেহটি আগুনের হাতে সমর্পিত হলো। তিনি ছিলেন নকশালবাড়ি আন্দোলন এবং ভারতের কমিউনিস্ট পার্টির (মার্ক্সিস্ট-লেনিনিস্ট) রাজনৈতিক এবং ভাবাদর্শের প্রণেতা। যে মানুষটি সর্বজনশ্রদ্ধেয়, সেই মানুষটিই সরকারের চোখে ছিলেন সবচে ভয়ংকর শত্রু। তাকে তুলে ধরা হয়েছিলো সহিংস বা নাশকতার রাজনীতির প্রতীক হিসেবে।</p>
									<p class="text-justify"> ঘটনাটি ঘটেছিলো আজ থেকে পঁয়তাল্লিশ বছর আগে। কিন্তু চিতা থেকে যে আগুনের শুরু তা আজো সাতান্ন বছর বয়েসী অভিজিৎ মজুমদারের স্মৃতিতে জ্বলছে। তিনি তার বাবার পা এক নজর দেখেছিলেন যে পায়ের স্মৃতি তার মনে জ্বলন্ত অঙ্গারের রেখা ফেলে যাচ্ছে আজও – তার বাবা মারা গেছিলেন পুলিশি হেফাজতে। তার পায়ের রং রূপান্তরিত হয়েছিলো কুচকালোতে। </p>

                                </div>
                            </div>

                        </div>

                        <!-- END .composs-panel -->
                    </div>

                </div>

                <!-- END .composs-main-content -->
            </div>

            <!-- BEGIN #sidebar -->
            <?php include_once 'sidebar.php';  ?>

<?php include_once ('footer.php'); ?>