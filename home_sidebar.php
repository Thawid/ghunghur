<!-- BEGIN #sidebar -->
<aside id="sidebar">

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3>আলোকচিত্র</h3>
        <div class="widget-content ot-w-gallery-list">

            <div class="item">
                <div class="item-header">
                    <div class="item-photo"><a href="javascript:void(0);"><img src="images/photos/galleryfour.jpg" alt="" /></a></div>
                    <div class="item-photo"><a href="javascript:void(0);"><img src="images/photos/galleryone.jpg" alt="" /></a></div>
                    <div class="item-photo"><a href="javascript:void(0);"><img src="images/photos/gallerytwo.JPG" alt="" /></a></div>
                    <div class="item-photo"><a href="javascript:void(0);"><img src="images/photos/gallerythree.jpg" alt="" /></a></div>
                </div>

            </div>

        </div>
        <!-- END .widget -->
    </div>

    <!-- BEGIN .widget -->
    <div class="widget">
        <h3>জনপ্রিয়  নিবন্ধন </h3>
        <div class="widget-content ot-w-article-list">

            <div class="item">
                <div class="item-header">

                    <a href="javascript:void(0);"><img src="images/photos/image-17.jpg" alt="" /></a>
                </div>
                <div class="item-content">
                    <h4><a href="javascript:void(0);">সিলভিয়া প্ল্যাথ ও তার কবিতা</a></h4>
                    <span class="item-meta">
												<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
											</span>
                </div>
            </div>

            <div class="item">
                <div class="item-header">

                    <a href="javascript:void(0);"><img src="images/photos/image-17.jpg" alt="" /></a>
                </div>
                <div class="item-content">
                    <h4><a href="javascript:void(0);">সিলভিয়া প্ল্যাথ ও তার কবিতা</a></h4>
                    <span class="item-meta">
												<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
											</span>
                </div>
            </div>

            <div class="item">
                <div class="item-header">

                    <a href="javascript:void(0);"><img src="images/photos/image-17.jpg" alt="" /></a>
                </div>
                <div class="item-content">
                    <h4><a href="javascript:void(0);">সিলভিয়া প্ল্যাথ ও তার কবিতা</a></h4>
                    <span class="item-meta">
												<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
											</span>
                </div>
            </div>

            <div class="item">
                <div class="item-header">

                    <a href="javascript:void(0);"><img src="images/photos/image-17.jpg" alt="" /></a>
                </div>
                <div class="item-content">
                    <h4><a href="javascript:void(0);">সিলভিয়া প্ল্যাথ ও তার কবিতা</a></h4>
                    <span class="item-meta">
												<span class="item-meta-item"><i class="material-icons">access_time</i>January 12, 2015</span>
											</span>
                </div>
            </div>

        </div>
        <!-- END .widget -->
    </div>
    <div class="widget">
        <h3> ভিডিও </h3>
        <div class="widget-content ot-w-gallery-list">

            <div class="item">
                <div class="item-header">
                    <div class="item-photo">
                        <iframe width="315" height="215" src="https://www.youtube.com/embed/PkUU8eDCVSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="item-photo">
                        <iframe width="315" height="215" src="https://www.youtube.com/embed/PkUU8eDCVSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="item-photo">
                        <iframe width="315" height="215" src="https://www.youtube.com/embed/PkUU8eDCVSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="item-photo">
                        <iframe width="315" height="215" src="https://www.youtube.com/embed/PkUU8eDCVSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>

            </div>

        </div>
        <!-- END .widget -->
    </div>
	<!-- BEGIN .widget -->
	<div class="widget">
		<div class="widget-content">
			<a href="#" target="_blank"><img src="images/photos/galleryfour.jpg" alt="" /></a>
		</div>
	<!-- END .widget -->
	</div>
    <!-- BEGIN .widget -->
    <div class="widget">
        <h3> নোটিস বোর্ড </h3>
        <div class="shortcode-content">
            <div class="ot-shortcode-accordion accordion">
                <div>
                    <a href="#"> বিজ্ঞান কল্পকাহিনির ২০০ বছর </a>
                    <div>
                        <p>
                            চল্লিশ দশকের কথা। নিজের গড়া দ্য মারকিউরি থিয়েটারে অরসন ওয়েলস প্রচার করলেন এইচ জি ওয়েলসের দ্য ওয়ার অব দ্য ওয়ার্ল্ডস উপন্যাসের ওপর ভিত্তি করে একটি রেডিও নাটক। ওই নাটকে বলা হয়েছিল, অন্য জগৎ থেকে এলিয়েনরা আক্রমণ করতে আসতে পারে।
                        </p>
                    </div>
                </div>
                <div>
                    <a href="#"> বিজ্ঞান কল্পকাহিনির ২০০ বছর </a>
                    <div>
                        <p>
                            চল্লিশ দশকের কথা। নিজের গড়া দ্য মারকিউরি থিয়েটারে অরসন ওয়েলস প্রচার করলেন এইচ জি ওয়েলসের দ্য ওয়ার অব দ্য ওয়ার্ল্ডস উপন্যাসের ওপর ভিত্তি করে একটি রেডিও নাটক। ওই নাটকে বলা হয়েছিল, অন্য জগৎ থেকে এলিয়েনরা আক্রমণ করতে আসতে পারে।
                        </p>
                    </div>
                </div>
                <div>
                    <a href="#"> বিজ্ঞান কল্পকাহিনির ২০০ বছর </a>
                    <div>
                        <p>
                            চল্লিশ দশকের কথা। নিজের গড়া দ্য মারকিউরি থিয়েটারে অরসন ওয়েলস প্রচার করলেন এইচ জি ওয়েলসের দ্য ওয়ার অব দ্য ওয়ার্ল্ডস উপন্যাসের ওপর ভিত্তি করে একটি রেডিও নাটক। ওই নাটকে বলা হয়েছিল, অন্য জগৎ থেকে এলিয়েনরা আক্রমণ করতে আসতে পারে।
                        </p>
                    </div>
                </div>
                <div>
                    <a href="#"> বিজ্ঞান কল্পকাহিনির ২০০ বছর </a>
                    <div>
                        <p>
                            চল্লিশ দশকের কথা। নিজের গড়া দ্য মারকিউরি থিয়েটারে অরসন ওয়েলস প্রচার করলেন এইচ জি ওয়েলসের দ্য ওয়ার অব দ্য ওয়ার্ল্ডস উপন্যাসের ওপর ভিত্তি করে একটি রেডিও নাটক। ওই নাটকে বলা হয়েছিল, অন্য জগৎ থেকে এলিয়েনরা আক্রমণ করতে আসতে পারে।
                        </p>
                    </div>
                </div>
                <div>
                    <a href="#"> বিজ্ঞান কল্পকাহিনির ২০০ বছর </a>
                    <div>
                        <p>
                            চল্লিশ দশকের কথা। নিজের গড়া দ্য মারকিউরি থিয়েটারে অরসন ওয়েলস প্রচার করলেন এইচ জি ওয়েলসের দ্য ওয়ার অব দ্য ওয়ার্ল্ডস উপন্যাসের ওপর ভিত্তি করে একটি রেডিও নাটক। ওই নাটকে বলা হয়েছিল, অন্য জগৎ থেকে এলিয়েনরা আক্রমণ করতে আসতে পারে।
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END .widget -->
    </div>

    <!-- END #sidebar -->
</aside>

</div>
<div class="row">
    <div class="disqus" id="disqus_thread"></div>
</div>

<!-- END .wrapper -->
</div>


<!-- BEGIN .content -->
</div>