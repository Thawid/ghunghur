<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');



Route::resource('users', 'Admin\UserController');

Auth::routes();

Route::get('home', 'HomeController@index', function(){})->name('home');
Route::get('admin/login', 'AdminController@index', function(){})->name('admin.login');

Route::any('admin/logout', 'Admin\LoginController@logout')->name('admin');

Route::get('logout', 'Auth\LoginController@logout');



Route::GET('admin/home','AdminController@index');
Route::GET('admin/client','ClientController@index');
      /* -------------All Route For Article --------------*/
Route::GET('admin/AllArtical','AdminController@AllArtical')->name('admin.AllArtical');
Route::PUT('update/{id}','AdminController@update')->name('admin.update');
Route::get('update/{id}','AdminController@edit')->name('admin.update');
Route::GET('view/{id}','AdminController@show')->name('admin.view');
Route::GET('destroy/{id}','AdminController@destroy')->name('admin.destroy');
Route::get('admin/Artical','AdminController@Artical')->name('admin.Artical');
Route::post('admin/CreateArtical','AdminController@CreateArtical')->name('admin.CreateArtical');
Route::get('admin/getAllArticle', 'AdminController@getAllArticle')->name('getAllArticle');
Route::get('unpublished/{id}', 'AdminController@unpublished')->name('admin.unpublished');
Route::get('published/{id}', 'AdminController@published')->name('admin.published');

/* -------------All Route For Poeam --------------*/

Route::get('admin/AddPoeams', 'PoeamsController@AddPoeams')->name('admin.AddPoeams');
Route::post('admin/StotePoeams', 'PoeamsController@StotePoeams')->name('admin.StotePoeams');
Route::get('admin/AllPoeams', 'PoeamsController@AllPoeams')->name('AllPoeams');
Route::get('admin/getAllPoeams', 'PoeamsController@getAllPoeams')->name('getAllPoeams');
Route::get('destroyPoeams/{id}', 'PoeamsController@destroyPoeams')->name('admin.destroyPoeams');
Route::PUT('updatePoeam/{id}', 'PoeamsController@updatePoeam')->name('admin.updatePoeam');
Route::get('editPoeam/{id}','PoeamsController@editPoeam')->name('admin.editPoeam');
Route::GET('viewPoeams/{id}','PoeamsController@viewPoeams')->name('admin.viewPoeams');
Route::get('unpublishedPoeam/{id}', 'PoeamsController@unpublishedPoeam')->name('admin.unpublishedPoeam');
Route::get('publishedPoeam/{id}', 'PoeamsController@publishedPoeam')->name('admin.publishedPoeam');

/* -------------All Route For Short Story --------------*/

Route::get('admin/AddStory', 'StoryController@AddStory')->name('admin.AddStory');
Route::post('admin/StoreStory', 'StoryController@StoreStory')->name('admin.StoreStory');

Route::get('admin/AllStory', 'StoryController@AllStory')->name('AllStory');
Route::get('admin/getAllStory', 'StoryController@getAllStory')->name('getAllStory');
Route::get('destroyStory/{id}', 'StoryController@destroyStory')->name('admin.destroyStory');

Route::PUT('updateStory/{id}', 'StoryController@updateStory')->name('admin.updateStory');
Route::get('editStory/{id}','StoryController@editStory')->name('admin.editStory');

Route::GET('viewStory/{id}','StoryController@viewStory')->name('admin.viewStory');

Route::get('unpublishedStory/{id}', 'StoryController@unpublishedStory')->name('admin.unpublishedStory');
Route::get('publishedStory/{id}', 'StoryController@publishedStory')->name('admin.publishedStory');







Route::get('admin/getLogDetails', 'HomeController@getLogDetails')->name('getLogDetails');
Route::get('admin/addVersion','VersionController@addVersion')->name('admin.addVersion');
Route::post('admin/storeVersion','VersionController@storeVersion')->name('admin.storeVersion');
Route::POST('deleteVersion/{id}','VersionController@deleteVersion')->name('admin.deleteVersion');

Route::get('getVersion', 'getVersionController@getVersion')->name('getVersion');

//Route::GET('admin/client/api','ApiController@index');

Route::get('admin/logHistory', 'AdminController@logHistory')->name('logHistory');

Route::get('admin/homePageLoginHistory', 'AdminController@getAllUserLoginHistory')->name('homePageLoginHistory');

Route::get('loginDetails', 'HomeController@UserLoginHistory')->name('loginDetails');

Route::get('homeLogDetails', 'HomeController@homeLogDetails')->name('homeLogDetails');

Route::get('UserAllSIP', 'HomeController@UserAllSIP')->name('UserAllSIP');

Route::get('UserAllSIPip', 'HomeController@UserAllSIPip')->name('UserAllSIPip');

Route::get('deleteIP/{id}','HomeController@deleteIP')->name('layouts.deleteIP');
Route::GET('admin','Admin\LoginController@showLoginForm')->name('admin.login');
Route::GET('addSipIP','SipIpController@addSipIP')->name('addSipIP');
Route::post('storesipip','SipIpController@storesipip')->name('storesipip');
Route::get('getIPStatus', 'HomeController@getIPStatus')->name('getIPStatus');

Route::get('deleteSIPIP/{id}','HomeController@deleteSIPIP')->name('layouts.deleteSIPIP');

Route::POST('admin','Admin\LoginController@login');

Route::POST('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::POST('admin-password/reset','Admin\ResetPasswordController@reset');
Route::GET('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
