<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 11:58 AM
 */
?>

@extends('admin.layout.master')



@section('title', 'GhunGhur::Admin')



@section('content_header')

    <h1>Client Dashboard</h1>

@stop



@section('content')

    @if (isset($clientIP))

        <p>Welcome to this beautiful client panel - </p>



        <p> Your IP Is : {{$clientIP}} , Please Wait For Sync..</p>



    @endif
    @if (isset($syncClientIP))

        <p>Welcome to this beautiful client panel - </p>
        <p> Your IP Is : {{$syncClientIP}} , is successfully Synced</p>


    @endif

@stop



@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop



@section('js')

    <script> console.log('Hi!'); </script>

@stop
