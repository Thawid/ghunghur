<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>-->
            <!-- Optionally, you can add icons to the links -->
            <li  class="active">
                <a href="{{url('admin/home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>
            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Artical </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Artical </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Artical </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Poeams </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllPoeams')) class="active" @endif><a
                                href="{{url('admin/AllPoeams')}}"><i class="fa fa-list"></i> All Poeams </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddPoeams')) class="active" @endif><a
                                href="{{url('admin/AddPoeams')}}"><i class="fa fa-plus-square-o"></i> New Poeams </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Short Story </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllStory')) class="active" @endif><a
                                href="{{url('admin/AllStory')}}"><i class="fa fa-list"></i> All Short Story </a>
                    </li>
                    <li @if(URL::current()==url('admin.AddStory')) class="active" @endif><a
                                href="{{url('admin/AddStory')}}"><i class="fa fa-plus-square-o"></i> New Short Story </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Series </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Series </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Series </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Interview </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Interviews </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Interview </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Painting </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Painting </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Painting </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Movies </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Movies </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Movies </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Drama </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Drama </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Drama </a>
                    </li>

                </ul>
            </li>

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-book"></i>
                    <span> Rendering </span>
                    <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('admin/AllArtical')) class="active" @endif><a
                                href="{{url('admin/AllArtical')}}"><i class="fa fa-list"></i> All Rendering </a>
                    </li>
                    <li @if(URL::current()==url('admin.Artical')) class="active" @endif><a
                                href="{{url('admin/Artical')}}"><i class="fa fa-plus-square-o"></i> New Rendering </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="{{url('logout')}}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> <span>Sign Out</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
