<?php
/**
 * Created by Arifur Rahman.
 * Date: 10/30/2018
 * Time: 6:44 PM
 */
?>
    <!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('web/favicon.ico')}}"/>
    @include('admin.layout.include.header')
    <title>@yield('title')</title>
    @yield('style')
    <style>
        .page-loader{
            position: fixed;
            top: 0;
            bottom: 0 !important;
            left: 0;
            right: 0;
            background: #00466f;
            z-index: 2000;
            overflow: hidden;
            height: 100% !important;
        }
        .page-spinner{
            height: 50px;
            width: 50px;
            border:5px solid #fff;
            border-radius: 100%;
            margin: 50vh auto;
            transform: translate(-50%);
            position: relative;
        }
        .page-spinner::after{
            position: absolute;
            content: "";
            top: -5px;
            left: -5px;
            height: 50px;
            width: 50px;
            border: 5px solid transparent;
            border-top: 5px solid #00adef;
            border-radius: 100%;
            transform: rotate(45deg);
            animation: wheel 1s ease-in-out infinite;

        }
        @-webkit-keyframes wheel {
            from  { transform: rotate(45deg); }
            to { transform: rotate(405deg); }
        }
        @-moz-keyframes wheel {
            from  { transform: rotate(45deg); }
            to { transform: rotate(405deg); }
        }
        @-o-keyframes wheel {
            from  { transform: rotate(45deg); }
            to { transform: rotate(405deg); }
        }
        @keyframes wheel {
            from  { transform: rotate(45deg); }
            to { transform: rotate(405deg); }
        }

    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="page-loader">
    <div class="page-spinner"></div>
</div>
<div class="wrapper">
{{--include the top menu bar--}}
@include('admin.layout.include.top_sidebar')
{{--include the left side bar--}}
@include('admin.layout.include.left_sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="col-md-3">
                @yield('header_left')
            </h1>
            <h3 class="col-md-5 text-center no-margin">@yield('header_title')</h3>
            <ol class="breadcrumb">
                @yield('header_right')
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>

            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        <!-- Small boxes (Stat box) -->
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{--include the footer--}}
    @include('admin.layout.include.footer')
</div>
@yield('modal')
<!-- ./wrapper -->
{{--link the js plugin--}}
@include('admin.layout.include.javascript_bar')
@yield('script')
<script>
    $(window).on('load',function () {
        $(".page-loader").fadeOut();
        $(".no-overflow-body").removeClass('no-overflow-body');
    });
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor

    })
</script>
<script>

    $("document").ready(function(){


        $("#unpublished").click(function(e){
            e.preventDefault();
            var dataString = 'status='+status;
            $.ajax({
                type: "POST",
                url :  "unpublished",
                data : dataString,
                success : function(data){
                    console.log(data);
                }
            },"html");

        });
    });
</script>
</body>
</html>

