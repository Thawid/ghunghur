<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 3:49 PM
 */
?>

@extends('admin.layout.master')

@section('title',"GhunGhur || Article View")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

    @if(session()->has('status'))
        <p class="alert alert-info">
            {{  session()->get('status') }}
        </p>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{--<a href="{{ route('users.index') }}" class="btn btn-success btn-xs">Back</a> --}}
                Article Details
                <a style="margin-left: 776px;" href="{{ route('admin.AllArtical') }}" class="btn-primary btn-sm">All Articles</a>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Author Name</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewArticle->author_name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Title</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewArticle->title }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Published</label>
                        <div class="col-sm-9">
                            <p class="form-control">{{ $viewArticle->published_date }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Article</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="10" cols="20">{!! strip_tags($viewArticle->articale_body)  !!}</textarea>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection

