<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VersionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function addVersion()
    {

        $getVersion = DB::table('version')->get();

        //dd($getVersion);

        return view('admin.addVersion', Compact('getVersion'));


    }

    public function storeVersion(Request $request)
    {
        $validatedData = Validator::make($request->all(), [

            'version_name' => 'required|unique:version,version_name',

        ]);
        if ($validatedData->fails()) {
            return redirect('admin/addVersion')
                ->withErrors($validatedData)
                ->withInput();
        }


        $version_name = $request->input('version_name');


        $insert[] = [
            'version_name' => $version_name,
            'date' => Carbon::now(),
            'status' => 0,

        ];

        DB::table('version')->insert($insert);

        //return view('layouts.addSipIP');
        return redirect('admin/addVersion')->with('message', 'New Version  Successfully Added');


    }


    public function deleteVersion($id)
    {
        DB::table('version')->where('id', '=', $id)->delete();

        return redirect("admin/addVersion")->with('message', 'Successfully Delete Version!!');

    }

    public function getVersion()
    {

        //$version_list = array();

        $allVersion = DB::table('version')->select('version_name', 'date', 'status')->where('status', '=', 0)->get();
        // dd($allVersion);

        foreach ($allVersion as $versionName) {

            $version_list[] = $versionName->version_name;
        }

        $version_list = json_encode($version_list);

        $allVersion = json_decode($version_list, true);


        foreach ($allVersion as $version_name) {

            //dd($version_name);

            if ($version_name == true) {

                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                    //Is it a proxy address
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }

                DB::table('server_details')->insert(
                    [
                        'version' => $version_name,
                        'ip' => $ip,
                        'date' => Carbon::now()
                    ]
                );

                $contents = $version_name . ' ' . $ip . ' ' . date('d/m/Y h:i:s A') ."\n";
               //   dd($contents);
               File::append('storage/log2.txt', $contents);
            }

        }

        //return view('admin.getVersion', Compact('allVersion'));


    }
}
