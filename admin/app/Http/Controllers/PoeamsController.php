<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PoeamsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    public function AddPoeams()
    {

        return view('admin.AddPoeams');
    }

    public function StotePoeams(Request $request)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required|unique:poeams,title',
            'poeams_body' => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect('admin/AddPoeams')
                ->withErrors($validateData)
                ->withInput();

        }
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/poeams'), $imageName);
        $input = [
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'published_date' => $request['published_date'],
            'poeams_body' => $request['poeams_body'],
            'date' => Carbon::now(),
            'image' => $imageName,
            'status' => 1,
        ];
        //dd($input);
        DB::table('poeams')->insert($input);
        return redirect('admin/AddPoeams')->with('message', 'Poeams Successfully Published');

    }


    public function AllPoeams()
    {
        $poeams = DB::table('poeams')->get();
        return view('admin.AllPoeams', compact('poeams'));
    }


    public function getAllPoeams(Request $request)
    {

        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $poeams = DB::table('poeams')->get();

        foreach ($poeams as $allPoeams) {


            if ($allPoeams->status == '1') {
                $unpublished = '<a class="btn btn-success btn-xs" id="unpublished"  href="' . route('admin.unpublishedPoeam', [$allPoeams->id]) . '"><i class="fa fa-pause"></i></a>';
            } else {
                $unpublished = '<a class="btn btn-danger btn-xs" id="unpublished"  href="' . route('admin.publishedPoeam', [$allPoeams->id]) . '"><i class="fa fa-pause"></i></a>';
            }

            $data[] = [

                $allPoeams->author_name, str_limit($allPoeams->title, 15), $allPoeams->published_date, str_limit($allPoeams->poeams_body, 20),
                '<img height="50" src="' . asset('images/poeams') . DIRECTORY_SEPARATOR . $allPoeams->image . '">',
                '<a class="btn btn-primary btn-xs" href="' . route('admin.viewPoeams', [$allPoeams->id]) . '"><i class="fa fa-eye "></i></a> 
                    <a class="btn btn-success btn-xs" href="' . route('admin.editPoeam', [$allPoeams->id]) . '"><i class="fa fa-edit "></i></a> 
                    <a class="btn btn-danger btn-xs" onclick="return confirm(\'Are you want to delete this poeams ?\');"  href="' . route('admin.destroyPoeams', [$allPoeams->id]) . '"><i class="fa fa-fw fa-trash-o"></i></a>'
                . ' ' . $unpublished,


            ];

        }

        return [
            'draw' => $draw,
            'data' => $data
        ];
    }


    public function destroyPoeams($id)
    {

        $delete = DB::table('poeams')->where('id', '=', $id)->first();

        File::delete(public_path('images/poeams' . $delete->image));
        DB::table('poeams')->where('id', '=', $id)->delete();
        return redirect("admin/AllPoeams")->with('message', 'Successfully Delete Poeams!!');

    }

    public function editPoeam($id)
    {
        //
        $updatePoeam = DB::table('poeams')->where('id', '=', $id)->first();
        return view('admin.updatePoeam', compact('updatePoeam'));
    }

    public function updatePoeam(Request $request, $id)
    {

        $validateData = Validator::make($request->all(), [

            'title' => 'required',
            'poeams_body' => 'required',
            'image' => 'file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validateData->fails()) {

            return redirect()->back()
                ->withErrors($validateData)
                ->withInput();

        }

        if ($request->hasFile('image')) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/poeams'), $imageName);

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'poeams_body' => $request['poeams_body'],
                'date' => Carbon::now(),
                'image' => $imageName,
                'status' => 1,
            ];
        }else{

            $inputs = [
                'author_name' => $request['author_name'],
                'title' => $request['title'],
                'published_date' => $request['published_date'],
                'poeams_body' => $request['poeams_body'],
                'date' => Carbon::now(),
                'status' => 1,
            ];
        }
        DB::table('poeams')->where('id', '=', $id)->update($inputs);


        //return redirect("admin.AllArtical")->with('message', 'Successfully Update Article!!');
        return redirect()->back()->with('message', 'Successfully Update Article!!');
    }


    public function viewPoeams($id){

        $viewPoeams = DB::table('poeams')->where('id', '=', $id)->first();
        return view('admin.viewPoeams', compact('viewPoeams'));
    }


    public function publishedPoeam($id){

        DB::table('poeams')->where('id', '=', $id)->update([

            'status' =>1,

        ]);

        return redirect("admin/AllPoeams")->with('message', 'Successfully Published!!');
    }


    public function unpublishedPoeam($id){

        DB::table('poeams')->where('id', '=', $id)->update([

            'status' =>0,

        ]);

        return redirect("admin/AllPoeams")->with('message', 'Successfully Unpublished!!');

    }
}
