<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datatables;
use App\User;

class UserLoginHistory extends Model
{

    protected  $fillable = ['adminid','ip','ip_history','log_ip_add_date','del_ip','log_ip_del_date','sip_ip','sip_ip_history','sip_ip_add_date','del_sip_ip','sip_delete_date','status','signature'];

    public function set_table($table){
        $this->table = $table;
    }
}
